import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

let url = "http://178.128.34.164:3002/questions";
let token =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiMDAwMDAwMDMiLCJpYXQiOjE1MzM2NDQwOTMsImV4cCI6MTU2NTA5MzY5M30.oMv_mQN6mAAmAVrRAozC7Ytk3omAye9P_TQ8Xyg3VOE";

export default new Vuex.Store({
  state: {
    questions: []
  },
  getters: {
    questions(state) {
      return state.questions;
    }
  },
  mutations: {
    loadQuestions(state, payload) {
      state.questions = payload;
    }
  },
  actions: {
    loadQuestions({ commit }) {
      const headers = new Headers();

      headers.append("Content-Type", "application/json; charset=utf-8");
      headers.append("x-access-token", token);
      fetch(url, {
        method: "GET", // *GET, POST, PUT, DELETE, etc.
        mode: "cors",
        headers: headers
      })
        .then(response => {
          return response.json();
        })
        .then(data => {
          console.log(data.questions[1]);
          let questions = [];
          for (let item = 0; item < data.questions.length; item++) {
            const survey = {
              question: data.questions[item],
              answer: "",
              status: false
            };
            console.log(survey);
            questions.push(survey);
          }
          console.log(questions);
          commit("loadQuestions", questions);
        });
    }
  }
});
